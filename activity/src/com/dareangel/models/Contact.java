package com.dareangel.models;

public class Contact {

    String name = "Not set";
    String[] contactNumber = {"Not set"};
    String[] address = {"Not set"};

    public Contact() {}

    public Contact(String _name, String[] _cNumber, String[] _address) {
        name = _name;
        contactNumber = _cNumber;
        address = _address;
    }

    public void setName(String _name) {
        this.name = _name;
    }

    public void setContactNumber(String[] _contactNumber) {
        this.contactNumber = _contactNumber;
    }

    public void setAddress(String[] _address) {
        this.address = _address;
    }

    public String getName() {
        return name;
    }

    public String[] getContactNumbers() {
        return contactNumber;
    }

    public String[] getAddresses() {
        return address;
    }
}
