package com.dareangel.models;

import java.util.ArrayList;

public class Phonebook {

    ArrayList<Contact> contacts = new ArrayList<>();


    public Phonebook() {}

    public Phonebook(ArrayList<Contact> _contacts) {
        contacts = _contacts;
    }

    public void addContact(Contact _contact) {
        contacts.add(_contact);
    }

    public void setContacts(ArrayList<Contact> _contacts) {
        this.contacts = _contacts;
    }

    public ArrayList<Contact> getContacts() {
        return contacts;
    }

    public int getSize() {
        return contacts.size();
    }
}