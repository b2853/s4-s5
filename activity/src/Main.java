import com.dareangel.models.Contact;
import com.dareangel.models.Phonebook;

public class Main {
    public static void main(String[] args) {
        // instantiating and declaring phonebook
        final Phonebook phonebook = new Phonebook();
        // instantiating and declaring contacts
        final Contact contact1 = new Contact(
            "John Doe",
            new String[]{"+639235847362", "+639475329583"},
            new String[]{"Buenavista City", "Caloocan City"}
        );
        final Contact contact2 = new Contact(
            "Rene Tajos Jr.",
            new String[]{"+639232227362", "+639477779583"},
            new String[]{"Cavite City", "Isabel Leyte"}
        );
        // adding contacts to phonebook
        phonebook.addContact(contact1);
        phonebook.addContact(contact2);
        // checks if phonebook is empty
        if (phonebook.getSize() == 0) {
            logln("Your phonebook is empty! Add a contact.");
            return;
        }

        // output all the contact's details
        for (Contact contact : phonebook.getContacts()) {
            String name = contact.getName();
            String[] regNums = contact.getContactNumbers();
            String[] regAddresses = contact.getAddresses();

            logln(name);
            logln("------------------------");
            logln(name + " has the following registered numbers:");
            for (String num : regNums) {
                logln(num);
            }
            logln("------------------------------------");
            logln(name + " has the following registered addresses:");
            for (String address : regAddresses) {
                logln(address);
            }
            logln("========================================");
        }
    }

    private static void logln(Object obj) {
        System.out.println(obj);
    }

    private static void log(Object obj) {
        System.out.print(obj);
    }
}